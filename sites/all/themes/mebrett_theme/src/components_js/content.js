var Container, PortfolioItem, React;

React = require('react');

Container = require('./utils/container');

PortfolioItem = require('./nodes/portfolio_item');

module.exports = React.createClass({
  render: function() {
    return React.createElement("section", {
      "id": "content"
    }, React.createElement(Container, null, React.createElement("h2", null, "What I\'ve been up to recently..."), React.createElement(PortfolioItem, null), React.createElement(PortfolioItem, null), React.createElement(PortfolioItem, null)));
  }
});
