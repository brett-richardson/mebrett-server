var BookPromo, ContactLinks, Container, React, SubNav;

React = require('react');

Container = require('./utils/container');

SubNav = require('./navigation/sub_nav');

BookPromo = React.createClass({
  render: function() {
    return React.createElement("p", null, "BookPromo");
  }
});

ContactLinks = React.createClass({
  render: function() {
    return React.createElement("p", null, "ContactLinks");
  }
});

module.exports = React.createClass({
  render: function() {
    return React.createElement("footer", null, React.createElement(Container, null, React.createElement(BookPromo, null), React.createElement("div", {
      "className": "row"
    }, React.createElement("div", {
      "className": "col-sm-6"
    }, React.createElement(SubNav, null)), React.createElement("div", {
      "className": "col-sm-6"
    }, React.createElement(ContactLinks, null)))));
  }
});
