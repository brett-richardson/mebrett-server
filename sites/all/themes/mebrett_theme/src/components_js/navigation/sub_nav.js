var React;

React = require('react');

module.exports = React.createClass({
  render: function() {
    return React.createElement("div", {
      "className": "row"
    }, React.createElement("div", {
      "className": "col-sm-6"
    }, React.createElement("a", {
      "className": "btn btn-link",
      "href": "#"
    }, "home"), React.createElement("a", {
      "className": "btn btn-link",
      "href": "#"
    }, "github"), React.createElement("a", {
      "className": "btn btn-link",
      "href": "#"
    }, "portfolio")), React.createElement("div", {
      "className": "col-sm-6"
    }, React.createElement("a", {
      "className": "btn btn-link",
      "href": "#"
    }, "skills"), React.createElement("a", {
      "className": "btn btn-link",
      "href": "#"
    }, "about"), React.createElement("a", {
      "className": "btn btn-link",
      "href": "#"
    }, "contact")));
  }
});
