var React, TagList;

React = require('react');

TagList = React.createClass({
  render: function() {
    return React.createElement("p", null, "TagList");
  }
});

module.exports = React.createClass({
  render: function() {
    return React.createElement("article", null, React.createElement("img", {
      "src": "http://placehold.it/800x250"
    }), React.createElement("h2", null, "PortfolioItem"), React.createElement("div", {
      "className": "row"
    }, React.createElement("div", {
      "className": "col-sm-7"
    }, React.createElement("p", null, "Vestibulum sit amet luctus ex.\nNullam in magna quis purus mollis commodo ac nec risus.\nSed vitae commodo mi. Sed quis posuere mi. Nam nisl ex, egestas\nut libero vel, rhoncus commodo velit. Nullam non metus ligula."), React.createElement("p", null, "Proin erat quam, vulputate tempus est vel, faucibus imperdiet leo.\nIn cursus mauris ut magna ultricies, a vestibulum turpis elementum.\nVestibulum vitae consectetur ex. Vivamus volutpat leo quis arcu\ntristique rutrum ac vel nisi.")), React.createElement(TagList, {
      "className": "col-sm-5"
    })));
  }
});
