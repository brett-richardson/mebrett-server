React   = require 'react'
Header  = require './header'
Content = require './content'
Footer  = require './footer'

module.exports = React.createClass
  render: ->
    <div>
      <Header currentPage=@props.currentState.page />
      <Content portfolioItems=@props.portfolioItems />
      <Footer currentPage=@props.currentState.page />
    </div>
