React         = require 'react'
HeadingTicker = React.createClass render: -> <p>HeadingTicker</p>
IconLink      = React.createClass render: -> <p>IconLink</p>

module.exports = React.createClass
  render: ->
    <div className="row">
      <div className="col-sm-7">
        <img src="http://placehold.it/400x300" />
      </div>

      <div className="col-sm-5">
        <h1>I'm Brett</h1>

        <HeadingTicker />

        <IconLink href="http://github.com/brett-richardson" icon="github">
          Github
        </IconLink>

        <IconLink href="/blog" icon="blog">
          Blog
        </IconLink>
      </div>
    </div>
