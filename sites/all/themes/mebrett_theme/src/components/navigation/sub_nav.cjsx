React = require 'react'

module.exports = React.createClass
  render: ->
    <div className="row">
      <div className="col-sm-6">
        <a className="btn btn-link" href="#">home</a>
        <a className="btn btn-link" href="#">github</a>
        <a className="btn btn-link" href="#">portfolio</a>
      </div>

      <div className="col-sm-6">
        <a className="btn btn-link" href="#">skills</a>
        <a className="btn btn-link" href="#">about</a>
        <a className="btn btn-link" href="#">contact</a>
      </div>
    </div>
